#! /usr/bin/python3
from time import sleep
import RPi.GPIO as GPIO

## Setup the GPIO for PWM on pin12 {GPIO18}

GPIO.setwarnings(False)                             # Disable warnings
GPIO.setmode(GPIO.BCM)                              # Broadcom pin-numbering scheme
GPIO.setup(23, GPIO.OUT)                            # Set up GPIO pin 23 as an output

# Create a PWM (Pulse Width Modulation) object on GPIO pin 23 with a frequency of 220 Hz
p = GPIO.PWM(23, 220)
p.stop()                                            # Stop the PWM

# Define a list of musical note frequencies
notes = [622.25, 830.61, 783.99, 523.25]
duration = [0.5, 0.5, 1, 1]                         # Define the duration of the musical notes
wait = [0.1, 0.1, 0.1, 0.1]                         # Define the waiting time between the musical notes

# Define the 'scale playing' function
def scale(tune, duty, duration, wait):
    for i in range(len(tune)):
        p.ChangeFrequency(tune[i])      # Set the frequency of the PWM signal
        p.start(duty)                   # Start the PWM with the specified duty cycle
        sleep(duration[i])              # Play the note for the specified duration
        p.stop()                        # Stop the PWM
        sleep(wait[i])                  # Pause before playing the next note

# Call the scale function with the defined parameters
scale(notes, 50, duration, wait)
sleep(0.7) 

## Cleanup the GPIO stuff...
GPIO.cleanup()